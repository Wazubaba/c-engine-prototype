# A U G H ! ?
Augh? Augh. Because science. Yes.

# D E P E N D E N C I E S
Please note, the versions are just the ones I had available via apt.
You may be able to use earlier versions but they are not tested so
YMMV :s

* SDL2 >= 2.0.2
* SDL2_Image >= 2.0.2
* SDL2_Mixer >= 2.0.2
* libAgar >= 1.5.0
* libconfig >= 1.4.9-2
* pkg-config >= 0.28
* X11 libraries
* standard build stuff, if you want to use a different compiler you
	just need to edit the Makefile and set CC.

# C O M P I L I N G
(**NOTE**: If you are not cloning from git, you can skip the first step)
* This project uses git submodules, so first issue
	`git submodule update --init --recursive` before trying to build.
* Run `make release` and it should build. Debug depends on clang for
	asan memory testing as I haven't been able to get it working in my
	version of gcc...

# L I N K S
* [slog(fork)](https://gitlab.com/Wazubaba/slog-fork)
* [libconfig](https://github.com/hyperrealm/libconfig)
* [SDL2](https://www.libsdl.org/)
* [SDL2_Image](https://www.libsdl.org/projects/SDL_image/)
* [SDL2_Mixer](https://www.libsdl.org/projects/SDL_mixer/)
* [libAgar](http://libagar.org/)


Possibly
* [SDL2_Net](https://www.libsdl.org/projects/SDL_net/)
* [SDL2_TTF](https://www.libsdl.org/projects/SDL_ttf/)

in the future?
