CC= gcc

WARNFLAGS= -Wall -Wextra -Werror
OPTIMIZATIONS= -Os
INCLUDE= -Iinclude -Isupport/slog/src -Isupport/refmap/include \
				 $(shell sdl2-config --cflags) \
				 $(shell pkg-config --cflags libconfig) \
				 $(shell agar-config --cflags)


CFLAGS= $(OPTIMIZATIONS) $(WARNFLAGS) $(INCLUDE)

LDFLAGS= -L/usr/local/lib -Wl,-rpath,/usr/local/lib -Wl,--enable-new-dtags \
				 -lpthread -lconfig -lSDL2 -lSDL2_mixer -lSDL2_image -lconfig 

BIN= engine.a

SRC= $(shell find src -type f -name "*.c") support/slog/src/slog.c \
		 support/refmap/src/refmap.c

HDR= support/slog/src/slog.h \
		 support/refmap/include/refmap.h

OBJ= $(patsubst %.c, %.o, $(SRC))

.PHONY: all clean

all: $(BIN)
	-$(shell cp -r include ../game/include/engine)
	-$(shell mkdir ../game/lib/engine)
	$(shell cp $(HDR) ../game/include)
	$(shell cp $(BIN) ../game/lib)

$(BIN): $(OBJ)
	ar cr $(BIN) $^

clean:
	-$(RM) $(OBJ)
	-$(RM) $(BIN)
	-$(RM) -r ../game/include/engine ../game/include/refmap.h ../game/include/slog.h
	-$(RM) -r ../game/lib/$(BIN)

