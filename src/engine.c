#include "engine.h"

struct EngineInfo* engine_init(struct EngineInfo* einfo)
{
	/* Try to load engine settings */
	if (engine_cfg_load(einfo) == 0)
		slog(0, SLOG_WARN, "Cannot load core configuration; using default values");

	slog(0, SLOG_INFO, "Engine initializing");

	/* Initialize SDL */
	if (SDL_Init(0) != 0)
	{
		slog(0, SLOG_FATAL, "Failed to initialize SDL: %s", SDL_GetError());
		return NULL;
	}

	if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0)
	{
		slog(0, SLOG_FATAL, "Failed to initialize video: %s", SDL_GetError());
		engine_shutdown(einfo);
	}

	if (SDL_InitSubSystem(SDL_INIT_AUDIO) != 0)
	{
		slog(0, SLOG_FATAL, "Failed to initialize audio: %s", SDL_GetError());
		engine_shutdown(einfo);
	}

	if (SDL_InitSubSystem(SDL_INIT_TIMER) != 0)
	{
		slog(0, SLOG_FATAL, "Failed to initialize timer: %s", SDL_GetError());
		engine_shutdown(einfo);
	}

	if (SDL_InitSubSystem(SDL_INIT_EVENTS) != 0)
	{
		slog(0, SLOG_FATAL, "Failed to initialize events: %s", SDL_GetError());
		engine_shutdown(einfo);
	}

	slog(1, SLOG_INFO, "Initialized SDL");

	/* Initialize SDL image */
	int imflags = IMG_INIT_PNG;
	if (!(IMG_Init(imflags) & imflags))
	{
		slog(0, SLOG_FATAL, "Failed to initialize SDL_Image: %s", IMG_GetError());
		engine_shutdown(einfo);
		return NULL;
	}
	slog(1, SLOG_INFO, "Initialized SDL Image");

	/* Initialize audio */
	int formats = MIX_INIT_OGG | MIX_INIT_MOD | MIX_INIT_FLAC | MIX_INIT_MP3;
	if (! (Mix_Init(formats) & formats))
	{
		slog(0, SLOG_FATAL, "Failed to initialize SDL_Mixer: %s", Mix_GetError());
		engine_shutdown(einfo);
		return NULL;
	}
	
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
	{
		slog(0, SLOG_ERROR, "Failed to open audio stream: %s", Mix_GetError());
		engine_shutdown(einfo);
		return NULL;
	}
	slog(1, SLOG_INFO, "Initialzied SDL_Mixer and opened audio stream");

	/* Initialize main window */
	einfo->window = SDL_CreateWindow("test", 100, 100, SCREEN_WIDTH_DEFAULT, SCREEN_HEIGHT_DEFAULT, SDL_WINDOW_SHOWN);
	if (einfo->window == NULL)
	{
		slog(0, SLOG_FATAL, "Failed to initialize window: %s", SDL_GetError());
		engine_shutdown(einfo);
		return NULL;
	}
	slog(1, SLOG_INFO, "Initialized main window");

	/* Initialize main renderer */
	einfo->renderer = SDL_CreateRenderer(einfo->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (einfo->renderer == NULL)
	{
		slog(0, SLOG_FATAL, "Failed to initialize renderer for main window: %s", SDL_GetError());
		engine_shutdown(einfo);
		return NULL;
	}
	slog(1, SLOG_INFO, "Initialized renderer");

	/* Initialize all the various managers */
	slog(0, SLOG_INFO, "Initializing engine resource databases");

	txm_init(&einfo->tex_m);
	slog(0, SLOG_NONE, "+-> Texture Manager");

	snd_init(&einfo->sound_m);
	slog(0, SLOG_NONE, "+-> Sound Manager");

	msc_init(&einfo->music_m);
	slog(0, SLOG_NONE, "+-> Music Manager");
	
	/* Set initialized flag */
	einfo->initialized = 1;

	slog(1, SLOG_INFO, "Engine initialization complete");
	slog(1, SLOG_INFO, "Loading definitions...");

	if (cfg_loadTextures(&einfo->tex_m, "data/textures.cfg", einfo->renderer) == NULL)
	{
		slog(0, SLOG_FATAL, "Failed to load texture definitions so shutting down");
		engine_shutdown(einfo);
		return NULL;
	}

	if (cfg_loadSounds(&einfo->sound_m, "data/sounds.cfg") == NULL)
	{
		slog(0, SLOG_FATAL, "Failed to load sound definitions so shutting down");
		engine_shutdown(einfo);
		return NULL;
	}

	if (cfg_loadMusic(&einfo->music_m, "data/music.cfg") == NULL)
	{
		slog(0, SLOG_FATAL, "Failed to load music definitions so shutting down");
		engine_shutdown(einfo);
		return NULL;
	}

	return einfo;
}

void engine_shutdown(struct EngineInfo* einfo)
{
	msc_destroy(&einfo->music_m);
	slog(0, SLOG_INFO, "Freed all music");

	snd_destroy(&einfo->sound_m);
	slog(0, SLOG_INFO, "Freed all sounds");

	txm_destroy(&einfo->tex_m);
	slog(0, SLOG_INFO, "Freed all textures");

	SDL_DestroyRenderer(einfo->renderer);
	slog(0, SLOG_INFO, "Renderer freed");

	SDL_DestroyWindow(einfo->window);
	slog(0, SLOG_INFO, "Main window freed");

	IMG_Quit();
	slog(0, SLOG_INFO, "SDL Image shutdown");

	Mix_CloseAudio();
	Mix_Quit();
	slog(0, SLOG_INFO, "SDL Mixer shutdown");

	SDL_QuitSubSystem(SDL_INIT_EVENTS);
	SDL_QuitSubSystem(SDL_INIT_TIMER);
	SDL_QuitSubSystem(SDL_INIT_AUDIO);
	SDL_QuitSubSystem(SDL_INIT_VIDEO);
	SDL_Quit();
	slog(0, SLOG_INFO, "SDL shutdown");

	einfo->initialized = 0;
	slog(0, SLOG_INFO, "Engine Shutdown");
}

