#include <config/texture_config.h>

struct TextureManager* cfg_loadTextures(struct TextureManager* txm, const char* path, SDL_Renderer* renderer)
{
	if (path == NULL)
	{
		slog(0, SLOG_FATAL, "Null path given to cfg_loadTextures");
		return NULL;
	}

	struct config_t cfg;
	config_init(&cfg);

	if (config_read_file(&cfg, path) == CONFIG_FALSE)
	{
		slog(0, SLOG_FATAL, "Error loading textures from file %s", path);
		slog(0, SLOG_NONE, "%s[%d]: %s", path, config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);
		return NULL;
	}

	struct config_setting_t* data;
	data = config_lookup(&cfg, "textures");
	if (data == NULL)
	{
		slog(0, SLOG_FATAL, "Failed to find texture definition");
		config_destroy(&cfg);
		return NULL;
	}

	size_t numLoaded = config_setting_length(data);

	size_t itr;
	for (itr = 0; itr < numLoaded; itr++)
	{
		char* name;
		char* texpath;

		struct config_setting_t* definition = config_setting_get_elem(data, itr);
		struct config_setting_t* entry;

		if (definition == NULL)
		{
			slog(0, SLOG_FATAL, "Internal error: Tried to access non-existant element in %s", path);
			config_destroy(&cfg);
			return NULL;
		}

		entry = config_setting_get_member(definition, "name");
		if (entry == NULL)
		{
			slog(0, SLOG_FATAL, "No name defined for texture entry %zu", itr);
			config_destroy(&cfg);
			return NULL;
		}
		name = (char*) config_setting_get_string(entry);

		entry = config_setting_get_member(definition, "path");
		if (entry == NULL)
		{
			slog(0, SLOG_FATAL, "No path defined for texture %s (entry %zu)", name, itr);
			config_destroy(&cfg);
			return NULL;
		}
		texpath = (char*) config_setting_get_string(entry);

		if (txm_register(txm, name, texpath, renderer) == NULL)
		{
			slog(0, SLOG_FATAL, "Unable to load texture %s, check your paths!", path);
			config_destroy(&cfg);
			return NULL;
		}
	}

	slog(1, SLOG_INFO, "Loaded %zu textures", numLoaded);

	config_destroy(&cfg);

	return txm;
}

