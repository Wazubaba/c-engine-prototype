#include <config/core.h>

unsigned char engine_cfg_load(struct EngineInfo* einfo)
{
	/* Initialize slog enough to at least use it */
	slog_set_level(5);
	slog_set_file_level(5);
	slog_set_to_file(1);
	slog_set_pretty(0);
	slog_set_filestamp(1);
	slog_set_td_safe(1);
	slog_set_fname("log/engine");

	/* Set default values */
	einfo->dataRoot = "data";
	einfo->mediaRoot = "media";
	einfo->screenWidth = 800;
	einfo->screenHeight = 600;

	struct config_t cfg;
	config_init(&cfg);

	if (config_read_file(&cfg, CONFIG_PATH_DEFAULT) == CONFIG_FALSE)
	{
		slog(0, SLOG_WARN, "Error loading engine configuration");
		slog(0, SLOG_NONE, "data/engine.cfg[%d]: %s", config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);
		return 0;
	}

	/* Get resolution */
	struct config_setting_t* element;
	struct config_setting_t* element2;

	element = config_lookup(&cfg, "video.resolution.w");
	element2 = config_lookup(&cfg, "video.resolution.h");

	if (element != NULL && element2 != NULL)
	{
		einfo->screenWidth = config_setting_get_int(element);
		einfo->screenHeight = config_setting_get_int(element2);
	}

	/* Get log settings */
	element = config_lookup(&cfg, "log.level");
	if (element != NULL)
		slog_set_level(config_setting_get_int(element));

	element = config_lookup(&cfg, "log.file");
	if (element != NULL)
		slog_set_to_file(config_setting_get_bool(element));

	element = config_lookup(&cfg, "log.colors");
	if (element != NULL)
		slog_set_pretty(config_setting_get_bool(element));

	element = config_lookup(&cfg, "log.fileTimeStamp");
	if (element != NULL)
		slog_set_filestamp(config_setting_get_bool(element));

	element = config_lookup(&cfg, "log.fileLevel");
	if (element != NULL)
		slog_set_file_level(config_setting_get_int(element));

	/* Get path settings */
	element = config_lookup(&cfg, "paths.data");
	if (element != NULL)
		einfo->dataRoot = (char*) config_setting_get_string(element);

	element = config_lookup(&cfg, "paths.media");
	if (element != NULL)
		einfo->mediaRoot = (char*) config_setting_get_string(element);

	config_destroy(&cfg);

	return 1;
}

unsigned char engine_cfg_save(struct EngineInfo* einfo)
{
	if (einfo == NULL)
		return 0;

	config_t cfg;
	config_setting_t *root, *setting, *member, *submember;

	config_init(&cfg);
	config_set_options(&cfg,
			(CONFIG_OPTION_COLON_ASSIGNMENT_FOR_GROUPS
			 | CONFIG_OPTION_OPEN_BRACE_ON_SEPARATE_LINE));

	if (!config_read_file(&cfg, CONFIG_PATH_DEFAULT))
	{
		slog(0, SLOG_WARN, "Error writing config");
		config_destroy(&cfg);
		return 0;
	}

	root = config_root_setting(&cfg);

	/* Handle Video Settings */
	setting = config_setting_get_member(root, "video");
	if (setting == NULL)
		setting = config_setting_add(root, "video", CONFIG_TYPE_GROUP);

	member = config_setting_get_member(setting, "resolution");
	if (member == NULL)
		member = config_setting_add(setting, "resolution", CONFIG_TYPE_GROUP);

	submember = config_setting_get_member(member, "w");
	if (submember == NULL)
		submember = config_setting_add(member, "w", CONFIG_TYPE_INT);
	config_setting_set_int(submember, einfo->screenWidth);

	submember = config_setting_get_member(member, "h");
	if (submember == NULL)
		submember = config_setting_add(member, "h", CONFIG_TYPE_INT);
	config_setting_set_int(submember, einfo->screenHeight);

	if (!config_write_file(&cfg, CONFIG_PATH_DEFAULT))
	{
		slog(0, SLOG_WARN, "Error writing to '%s'", CONFIG_PATH_DEFAULT);
		config_destroy(&cfg);
		return 0;
	}

	slog(0, SLOG_INFO, "Wrote new settings to '%s'", CONFIG_PATH_DEFAULT);
	config_destroy(&cfg);
	return 1;
}

