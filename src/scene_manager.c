#include "scene_manager.h"

void scm_init(struct SceneManager* scm)
{
	scm->contents = NULL;
	scm->length = 0;
	puts("Initialised Scene Manager");
}

struct Vis* scm_register(struct SceneManager* scm, struct Vis* entity)
{
	if (entity == NULL)
		return NULL;

	struct Vis** tcont = realloc(scm->contents, sizeof(struct Vis*) * (scm->length + 1));
	if (tcont != NULL)
		scm->contents = tcont;
	else
		return NULL;

	/* o_O VproblemV why?*/
	scm->contents[scm->length] = entity;
	scm->length += 1;

	return entity;
}

void scm_draw(struct SceneManager* scm, SDL_Renderer* renderer)
{
	size_t itr;
	SDL_RenderClear(renderer);

	for (itr = 0; itr < scm->length; itr++)
	{
		printf("Blitting entity %zu...\n", itr);
		struct Vis* entity = scm->contents[itr];
		if (entity->texture == NULL)
			puts("TEXTURE IS NULL");
		if (renderer == NULL)
			puts("RENDERER IS NULL");
		SDL_RenderCopy(renderer, entity->texture, &entity->tilesheet_info, &entity->transform);
	}

	SDL_RenderPresent(renderer);
}

void scm_destroy(struct SceneManager* scm)
{
	free(scm->contents);
	scm->contents = NULL;
	scm->length = 0;
}

