#include "visual_instance.h"

void vis_init(struct Vis* vis, SDL_Texture* tex, const int cellWidth, const int cellHeight)
{
	vis->texture = tex;
	int w, h;

	SDL_QueryTexture(vis->texture, NULL, NULL, &w, &h);

	vis->bounds.w = w;
	vis->bounds.h = h;

	vis->transform.x = 0;
	vis->transform.y = 0;

	/* Assume that cell size is good enough for a default scale */
	vis->transform.w = cellWidth;
	vis->transform.h = cellHeight;

	vis->tilesheet_info.x = 0;
	vis->tilesheet_info.y = 0;
	vis->tilesheet_info.w = cellWidth;
	vis->tilesheet_info.h = cellHeight;

	vis->cellWidth = cellWidth;
	vis->cellHeight = cellHeight;

	/* Do not initialize animations */
	vis->isAnimated = 0;
	vis->frameCount = 0;
	vis->frameDelay = 0;
	vis->currentFrame = 0;
	vis->frameRate = 0;
}

void vis_init_animations(struct Vis* vis, const int frames, const int delay)
{
	vis->isAnimated = 1;
	vis->frameCount = frames;
	vis->frameDelay = delay;
}

void vis_calcAnimation(struct Vis* vis)
{
	if (!vis->isAnimated)
		return;

	if (vis->frameRate >= vis->frameDelay)
	{
		vis->currentFrame += 1;
		vis->frameRate = 0;

		if (vis->currentFrame >= vis->frameCount)
		{
			/* Reset to the begining */
			vis->tilesheet_info.x = 0;
			vis->tilesheet_info.y = 0;
			vis->currentFrame = 0;
			return;
		}

		/* Move the tilesheet_info rect, but ensure it stays within image bounds */
		if (vis->tilesheet_info.x + vis->cellWidth >= vis->bounds.w)
		{
			vis->tilesheet_info.x = 0;
			vis->tilesheet_info.y += vis->cellHeight;
			if (vis->tilesheet_info.y > vis->bounds.h)
				vis->tilesheet_info.y = 0;
		}
		else
			vis->tilesheet_info.x += vis->cellWidth;
	}
	else
	{
		vis->frameRate += 1;
		return;
	}
}

