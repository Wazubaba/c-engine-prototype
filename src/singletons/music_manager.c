#include <singletons/music_manager.h>

void msc_cleanup(void* data)
{
	Mix_FreeMusic((Mix_Music*) data);
}

struct MusicManager* msc_init(struct MusicManager* msc)
{
	unsigned char isAlloc = 0;

	if (msc == NULL)
	{
		msc = malloc(sizeof(struct MusicManager));
		if (msc == NULL)
			return NULL;

		isAlloc = 1;
	}

	if (rmap_init(&msc->refmap, msc_cleanup) == NULL)
	{
		if (isAlloc)
			free(msc);
		return NULL;
	}

	msc->onHeap = isAlloc;

	return msc;
}

void msc_destroy(struct MusicManager* msc)
{
	if (msc != NULL)
	{
		unsigned char shouldFree = msc->onHeap;
		rmap_destroy(&msc->refmap);

		msc->onHeap = 0;

		if (shouldFree)
			free(msc);
	}
}

Mix_Music* msc_register(struct MusicManager* msc, const char* name, const char* path)
{
	/* Ensure we can even load the file first :P */
	Mix_Music* music = Mix_LoadMUS(path);
	if (music == NULL)
		return NULL;

	void* rhs = rmap_register(&msc->refmap, name, (void*) music);

	if (rhs != NULL)
		slog(2, SLOG_NONE, "[%s] Loaded music '%s'", strclr(CLR_NAGENTA, "LOAD"), path);

	return (Mix_Music*) rhs;
}

Mix_Music* msc_lookup(struct MusicManager* msc, const char* name)
{
	void* rhs = rmap_lookup(&msc->refmap, name);
	return (Mix_Music*) rhs;
}

