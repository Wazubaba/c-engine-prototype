#include <singletons/sound_manager.h>

void snd_cleanup(void* data)
{
	Mix_FreeChunk((Mix_Chunk*) data);
}

struct SoundManager* snd_init(struct SoundManager* snd)
{
	unsigned char isAlloc = 0;

	if (snd == NULL)
	{
		snd = malloc(sizeof(struct SoundManager));
		if (snd == NULL)
			return NULL;

		isAlloc = 1;
	}

	if (rmap_init(&snd->refmap, snd_cleanup) == NULL)
	{
		if (isAlloc)
			free(snd);
		return NULL;
	}

	snd->onHeap = isAlloc;

	return snd;
}

void snd_destroy(struct SoundManager* snd)
{
	if (snd != NULL)
	{
		unsigned char shouldFree = snd->onHeap;
		rmap_destroy(&snd->refmap);

		snd->onHeap = 0;

		if (shouldFree)
			free(snd);

	}
}

Mix_Chunk* snd_register(struct SoundManager* snd, const char* name, const char* path)
{
	/* Ensure we can even load the file first :P */
	Mix_Chunk* sound = Mix_LoadWAV(path);
	if (sound == NULL)
		return NULL;

	void* rhs = rmap_register(&snd->refmap, name, sound);

	if (rhs != NULL)
		slog(2, SLOG_NONE, "[%s] Loaded sound '%s'", strclr(CLR_NAGENTA, "LOAD"), path);

	return (Mix_Chunk*) rhs;
}

Mix_Chunk* snd_lookup(struct SoundManager* snd, const char* name)
{
	void* rhs = rmap_lookup(&snd->refmap, name);
	return (Mix_Chunk*) rhs;
}

