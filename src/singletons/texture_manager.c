#include <singletons/texture_manager.h>

void txm_cleanup(void* data)
{
	SDL_DestroyTexture((SDL_Texture*) data);
}

struct TextureManager* txm_init(struct TextureManager* txm)
{
	unsigned char isAlloc = 0;

	if (txm == NULL)
	{
		txm = malloc(sizeof(struct TextureManager));
		if (txm == NULL)
			return NULL;

		isAlloc = 1;
	}

	if (rmap_init(&txm->refmap, txm_cleanup) == NULL)
	{
		if (isAlloc)
			free(txm);
		return NULL;
	}

	txm->onHeap = isAlloc;

	return txm;
}

void txm_destroy(struct TextureManager* txm)
{
	if (txm != NULL)
	{
		unsigned char shouldFree = txm->onHeap;
		rmap_destroy(&txm->refmap);

		txm->onHeap = 0;

		if (shouldFree)
			free(txm);
	}
}

SDL_Texture* txm_register(struct TextureManager* txm, const char* name, const char* path, SDL_Renderer* renderer)
{
	/* Ensure we can even load the file first :P */
	SDL_Texture* tex = loadTexture(path, renderer);
	if (tex == NULL)
		return NULL;

	void* rhs = rmap_register(&txm->refmap, name, (void*)tex);

	if (rhs != NULL)
		slog(2, SLOG_NONE, "[%s] Loaded texture '%s'", strclr(CLR_NAGENTA, "LOAD"), path);

	return (SDL_Texture*) rhs;
}

SDL_Texture* txm_lookup(struct TextureManager* txm, const char* name)
{
	void* rhs = rmap_lookup(&txm->refmap, name);
	return (SDL_Texture*) rhs;
}

