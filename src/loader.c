#include "loader.h"

SDL_Texture* loadTexture(const char* path, SDL_Renderer* renderer)
{
	SDL_Texture* texture = NULL;
	SDL_Surface* surf = IMG_Load(path);
	if (surf == NULL)
		slog(0, SLOG_ERROR, "Unable to load image %s, Error: %s\n", path, IMG_GetError());
	else
	{
		texture = SDL_CreateTextureFromSurface(renderer, surf);
		if (texture == NULL)
			slog(0, SLOG_ERROR, "Error creating texture for image: %s\n", path, SDL_GetError());
		
		SDL_FreeSurface(surf);
	}

	return texture;
}

