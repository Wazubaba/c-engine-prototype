#ifndef ENGINE_H
#define ENGINE_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <slog.h>
#include <libconfig.h>

#include "engine_types.h"
#include "config/core.h"

#include "singletons/texture_manager.h"
#include "singletons/sound_manager.h"
#include "singletons/music_manager.h"

#include "config/texture_config.h"
#include "config/sound_config.h"
#include "config/music_config.h"

/**
 * Initialize all engine subsystems
 * @param einfo Pointer to the EngineInfo struct to initialize
 * @return pointer to newly-initialized EngineInfo struct or NULL on
 * failure
*/
struct EngineInfo* engine_init(struct EngineInfo* einfo);

/**
 * Shutdown all subsystems and free memory. This function is safe to
 * call even if the engine is not initialized.
 * @param pointer to the engine info
*/
void engine_shutdown(struct EngineInfo* einfo);


#endif /* ENGINE_H */

