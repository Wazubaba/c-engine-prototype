#ifndef SOUND_CONFIG_H
#define SOUND_CONFIG_H

#include <SDL_mixer.h>
#include <libconfig.h>
#include <slog.h>
#include <malloc.h>
#include "singletons/sound_manager.h"

struct SoundManager* cfg_loadSounds(struct SoundManager* snd, const char* path);

#endif /* SOUND_CONFIG_H */

