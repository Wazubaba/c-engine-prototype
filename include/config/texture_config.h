#ifndef TEXTURE_CONFIG_H
#define TEXTURE_CONFIG_H

#include <SDL.h>
#include <slog.h>
#include <libconfig.h>
#include <malloc.h>
#include "singletons/texture_manager.h"

struct TextureManager* cfg_loadTextures(struct TextureManager* txm, const char* path, SDL_Renderer* renderer);

#endif /* TEXTURE_CONFIG_H */

