#ifndef CORE_H
#define CORE_H

#include <slog.h>
#include <string.h>
#include <libconfig.h>

#include <engine_types.h>

/**
 * Load configuration for the engine from a file
 * @param einfo EngineInfo struct to initialize
 * @return 1 on success, 0 on failure
*/
unsigned char engine_cfg_load(struct EngineInfo* einfo);

/**
 * save configuration for the engine to a file
 * @param einfo EngineInfo struct to save from
 * @return 1 on success, 0 on failure
 * @note Only saves video settings and engine-related things
*/
unsigned char engine_cfg_save(struct EngineInfo* einfo); 

#endif /* CORE_H */

