#ifndef MUSIC_CONFIG_H
#define MUSIC_CONFIG_H

#include <SDL_mixer.h>
#include <libconfig.h>
#include <slog.h>
#include <malloc.h>
#include "singletons/music_manager.h"

struct MusicManager* cfg_loadMusic(struct MusicManager* msc, const char* path);

#endif /* MUSIC_CONFIG_H */

