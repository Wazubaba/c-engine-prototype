#ifndef LOADER_H
#define LOADER_H

#include <SDL.h>
#include <SDL_image.h>
#include <slog.h>
/**
 * Load a target texture file via SDL Image
 * @param path path to the image to load
 * @param renderer renderer to load the texture onto
 * @return the loaded texture, or NULL if something went wrong
*/
SDL_Texture* loadTexture(const char* path, SDL_Renderer* renderer);

#endif /* LOADER_H */

