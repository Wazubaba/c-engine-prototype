#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <SDL.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <slog.h>

#include <refmap.h>

#include "loader.h"

struct TextureManager
{
	struct RefMap refmap;
	unsigned char onHeap;
};

/**
 * Create a new texture manager
 * @param txm pointer to the texture manager to create
 * @return pointer to the new texture manager or null on failure
*/
struct TextureManager* txm_init(struct TextureManager* txm);

/**
 * Destroy a texture manager
 * @param txm Texture manager to destroy
*/
void txm_destroy(struct TextureManager* txm);

/**
 * Load a texture from a target file
 * @param txm Texture manager to load data into
 * @param name internal name of texture
 * @param path path to the texture to load
 * @param renderer rendere to create texture for
 * @return pointer to the new texture on success or NULL on failure
*/
SDL_Texture* txm_register(struct TextureManager* txm, const char* name, const char* path, SDL_Renderer* renderer);

/**
 * Query the database for a texture
 * @param txm texture database to query
 * @param name name of texture to look up
 * @return pointer to texture on success or NULL on failure
*/
SDL_Texture* txm_lookup(struct TextureManager* txm, const char* name);


#endif /* TEXTURE_MANAGER_H */

