#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include <SDL_mixer.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <slog.h>

#include <refmap.h>

#include "loader.h"

struct SoundManager
{
	struct RefMap refmap;
	unsigned char onHeap;
};

/**
 * Create a new sound manager
 * @param snd pointer to the sound manager to create
 * @return pointer to the new sound manager or null on failure
*/
struct SoundManager* snd_init(struct SoundManager* snd);

/**
 * Destroy a sound manager
 * @param snd Sound manager to destroy
*/
void snd_destroy(struct SoundManager* snd);

/**
 * Load a sound from a target file
 * @param snd Sound manager to load data into
 * @param name internal name of sound
 * @param path path to the sound to load
 * @return pointer to the new sound on success or NULL on failure
*/
Mix_Chunk* snd_register(struct SoundManager* snd, const char* name, const char* path);

/**
 * Query the database for a sound
 * @param snd sound database to query
 * @param name name of sound to look up
 * @return pointer to sound on success or NULL on failure
*/
Mix_Chunk* snd_lookup(struct SoundManager* snd, const char* name);


#endif /* SOUND_MANAGER_H */

