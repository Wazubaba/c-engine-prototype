#ifndef MUSIC_MANAGER_H
#define MUSIC_MANAGER_H

#include <SDL_mixer.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <slog.h>

#include <refmap.h>

#include "loader.h"

struct MusicManager
{
	struct RefMap refmap;
	unsigned char onHeap;
};

/**
 * Create a new music manager
 * @param msc pointer to the music manager to create
 * @return pointer to the new music manager or null on failure
*/
struct MusicManager* msc_init(struct MusicManager* msc);

/**
 * Destroy a music manager
 * @param msc Music manager to destroy
*/
void msc_destroy(struct MusicManager* msc);

/**
 * Load a music from a target file
 * @param msc Music manager to load data into
 * @param name internal name of music
 * @param path path to the music to load
 * @return pointer to the new music on success or NULL on failure
*/
Mix_Music* msc_register(struct MusicManager* msc, const char* name, const char* path);

/**
 * Query the database for a music
 * @param msc music database to query
 * @param name name of music to look up
 * @return pointer to music on success or NULL on failure
*/
Mix_Music* msc_lookup(struct MusicManager* msc, const char* name);


#endif /* MUSIC_MANAGER_H */

