#ifndef TURRET_MANAGER_H
#define TURRET_MANAGER_H

#include <malloc.h>
#include <slog.h>

#include <refmap.h>

#include "resource/turret.h"

struct TurretManager
{
	struct RefMap refmap;
	unsigned char onHeap;
};

/**
 * Initialize a turret manager with no definitions.
 * @param turretman pointer to the turret manager to use
 * @return pointer to the new turret manager or NULL on failure
*/
struct TurretManager* turretman_init(struct TurretManager* turretman);

/**
 * Destroy a turret manager and free all of the resources managed by it.
 * @param turretman pointer to the turret manager to use
*/
void turretman_destroy(struct TurretManager* turretman);

/**
 * Register a turret to the manager.
 * @param turretman pointer to the turret manager to use
 * @param turret pointer to the turret to be managed
 * @return pointer to the newly registered turret or NULL on failure
*/
struct Turret* turretman_register(struct TurretManager* turretman, const char* name, struct Turret* turret);

/**
 * Query the manager for a turret specified by name.
 * @param turretman pointer to the turret manager to use
 * @param name name of the turret to query for
 * @return pointer to the found turret or NULL on failure
*/
struct Turret* turretman_lookup(struct TurretManager* turretman, const char* name);


#endif /* TURRET_MANAGER_H */

