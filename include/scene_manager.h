#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include <stdio.h>
#include <malloc.h>
#include <SDL.h>

#include "visual_instance.h"

struct SceneManager
{
	/** Array of entities that will be drawn to the screen */
	struct Vis** contents;

	/** Number of entities that will be drawn to the screen */
	size_t length;
};

/**
 * Initialise a new scene manager
 * @param scm Pointer to the scene manager to initialise
*/
void scm_init(struct SceneManager* scm);

/**
 * Destroy and free all data within a scene manager
 * @param scm Pointer to the scene manager to destroy
*/
void scm_destroy(struct SceneManager* scm);

/**
 * Register an entity to a scene manager
 * @param scm Pointer to the scene manager to append to
 * @param vis Pointer to the visual data of a given entity that needs to be drawn
 * @return Pointer to the appeneded entity on success or NULL on failure
*/
struct Vis* scm_register(struct SceneManager* scm, struct Vis* vis);

/**
 * Draw all registered entities
 * @param scm Scene manager to draw the entities within
 * @param renderer renderer to draw the entities to
*/
void scm_draw(struct SceneManager* scm, SDL_Renderer* renderer);


#endif /* SCENE_MANAGER_H */

