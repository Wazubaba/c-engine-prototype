#ifndef ENGINE_TYPES_H
#define ENGINE_TYPES_H

#include <SDL.h>
#include <slog.h>
#include "singletons/texture_manager.h"
#include "singletons/sound_manager.h"
#include "singletons/music_manager.h"

#define SCREEN_WIDTH_DEFAULT 800
#define SCREEN_HEIGHT_DEFAULT 600
#define DATA_PATH_DEFAULT "data"
#define MEDIA_PATH_DEFAULT "media"
#define CONFIG_PATH_DEFAULT "data/engine.cfg"

struct EngineInfo
{
	/** Status of the engine */
	unsigned char initialized;

	char* dataRoot;
	char* mediaRoot;

	int screenWidth;
	int screenHeight;

	/** Reference to the renderer */
	SDL_Renderer* renderer;

	/** Reference to the window */
	SDL_Window* window;

	/** Reference to the texture manager */
	struct TextureManager tex_m;

	/** Reference to the sound manager */
	struct SoundManager sound_m;

	/** Reference to the music manager */
	struct MusicManager music_m;
};

#endif /* ENGINE_TYPES_H */

