#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <SDL.h>
#include <slog.h>
/* TODO:
 * Determine whether current method of L2R-T2D animation is enough or
 * if we should expose this via configs and allow an animation to be
 * defined across multiple images and totally defined in data.
 *
 * Internally this alternative method would be stored as an array of
 * struct Frame {SDL_Rect offset, SDL_Texture* tex} that would be
 * iterated each cycle.
 *
 * Benefit would be totally customisable animations via data, at the
 * cost of more memory usage and slightly longer load times.
*/
struct Vis
{
	/** Transform for the entire sprite */
	SDL_Rect transform;

	/** Offset information for the entire sprite */
	SDL_Rect tilesheet_info;

	/** Bounds of the image used for animation */
	SDL_Rect bounds;

	/** Texture informat */
	SDL_Texture* texture;

	/** Is the texture animated */
	unsigned char isAnimated;

	/** Width of an animation cell */
	int cellWidth;

	/** Height of an animation cell */
	int cellHeight;

	/** Number of frames in sheet */
	int frameCount;

	/** Delay between each frame of animation */
	int frameDelay;

	/** Internal storage for current frame number */
	int currentFrame;
	/** Internal storage for tracking delay remaining before incrementing
	 * to next frame of animation
	*/
	int frameRate;
};

/**
 * Initialize a new Vis struct with default data from an SDL_Texture
 * @param vis Vis struct to initialize
 * @param tex Texture sheet to use for the Vis
 * @param cellWidth width of a cell on the sheet
 * @param cellHeight height of a cell on the sheet
*/
void vis_init(struct Vis* vis, SDL_Texture* tex, const int cellWidth, const int cellHeight);

/**
 * Helper to initialize animation for an already initialized vis struct
 * @param vis Vis struct to initialize
 * @param frames Number of frames of animation (left to right, top to bottom)
 * @param delay Number of frames between animation changes
*/
void vis_init_animations(struct Vis* vis, const int frames, const int delay);

/**
 * Update the animation of a target Vis.
 * @param entity Visual instance to animate
*/
void vis_calcAnimation(struct Vis* vis);

#endif /* DRAWABLE_H */

